package polymorphism;

public class ElectronicBook extends Book {
    private double numberBytes;

    public ElectronicBook(String title, String author, double numBytes){
        super(title, author);
        this.numberBytes = numBytes;
    }

    public double getNumberBytes() {
        return this.numberBytes;
    }

    @Override
    public String toString(){
        String fromBase = super.toString();
        return fromBase + ", Size: "+ this.numberBytes +" bytes";
    }
}